kind: junction
description: >-
  Junction to freedesktop-sdk.

  A junction element allows us to access another bst project as if it was
  local here, like a repository_rule in bazel. 

  freedesktop-sdk is a project to build a basic flatpak runtime, which as a
  result means it's a pretty minimal GNU/Linux based toolchain, easily
  consumable by other BuildStream projects.

config:
  # Set the `target_arch` option in freedesktop-sdk to the same as here
  #
  options:
    target_arch: '%{arch}'

  # Causes artifacts from elements in freedesktop-sdk to be cached in our
  # remote cache. This is necessary thanks to incompatibilities in cache
  # servers between the BuildStream versions we're using.
  #
  cache-junction-elements: True

  # Thanks to a bug in BuildStream we can't set this to True and we will
  # keep getting warnings about cache incompatibilities. Once
  # https:/gitlab.com/buildstream/buildstream/merge_requests/1759 lands this
  # can be changed to True. This will stop bst polling the incompatible
  # freedesktop-sdk cache server.
  #
  ignore-junction-remotes: False

sources:
- kind: git
  url: https://gitlab.com/freedesktop-sdk/freedesktop-sdk.git
  track: valentindavid/bst2-and-bootable
  ref: 700ddae24697dc47a26295d4c5e6ef69cfc45322

  # The git and git_tag plugins can handle git submodules, this tells the
  # plugin about the existence of a submodule, and that it is not needed
  # for the purposes of this project.
  #
  # The submodule in question here is a script used by freedesktop-sdk to
  # check for ABI incompatibilities between updates.
  #
  submodules:
    utils/buildstream-abi-checker:
      checkout: False
      url: https://gitlab.com/freedesktop-sdk/buildstream-abi-checker.git

