# burn-sdk

A reference [BuildStream](https://buildstream.build) project to build and
integrate a [Bazel](https://bazel.build) application into a consumable disk
image. Feel free to explore, the project should be documented reasonably, but
if you're still confused feel free to ask questions on our IRC channel, which
you can find at #celduin on freenode.

Most of the heavy lifting is done in [freedesktop-sdk](https://freedesktop-sdk.io),
this repo pretty much just throws a couple of extra things into a reference
bootable image from freedesktop-sdk.

The application is sourced [here](https://gitlab.com/celduin/bazel-gtk-hello),
but the manner we get GTK into bazel is very hacky. At present this is more a
"proof of concept" that we _can_ integrate some Bazel project into a
BuildStream produced image.

## Install the dependencies

In order to build the bootable image you will need to install the necessary
dependencies. These are:

* [BuildStream master](https://buildstream.build/install.html) (Confirmed to build at 1.91.2+195.g05aaf3ead)
  - Thus you also need [buildbox-casd](https://buildstream.build/buildbox_install.html)
* [bst-plugins-experimental](https://gitlab.com/buildstream/bst-plugins-experimental) (At least 0.12.0-189-g99c5f36)
* pytoml, requests, PyGObject
  - These can be installed via pip: `pip3 install pytoml requests PyGObject`

## Build the bootable image

To build the bootable image, simply run:

```shell
bst build vm/image.bst
bst artifact checkout --hardlinks vm/image.bst
```

## Running the virtual machine

First make a copy of EFI variable image. On Debian for example:

```
cp /usr/share/OVMF/OVMF_VARS.fd efi_vars.fd
```

Alternatively, it could be in `/usr/share/qemu/edk2-i386-vars.fd`.

Then run:

```shell
qemu-system-x86_64 \
  -enable-kvm -m 2G \
  -vga virtio \
  -drive if=pflash,format=raw,unit=0,file=/usr/share/OVMF/OVMF_CODE.fd,readonly=on \
  -drive if=pflash,format=raw,unit=1,file=efi_vars.fd \
  -netdev user,id=net1 -device e1000,netdev=net1 \
  -display gtk,gl=on \
  -full-screen \
  -soundhw hda \
  -usb -device usb-tablet \
  -drive file=vm/image/disk.qcow2,format=qcow2,media=disk
```

Change the path to `/usr/share/OVMF/OVMF_CODE.fd` if your distribution
has a different path. It could be
`/usr/share/qemu/edk2-x86_64-code.fd`.

Some distributions do not ship OVMF images at all, in which case they may be
obtained [here](https://www.kraxel.org/repos/jenkins/edk2/). Using the pure-efi OVMF images provided in the
edk2.git-ovmf-x64 package should make things work. These are RPMs, so you may
need to find a tool that can handle this format.

## Contact us

If you need help or have any questions about the work done here, feel free to
join us on IRC: #celduin on freenode.
